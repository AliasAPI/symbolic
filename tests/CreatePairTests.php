<?php

namespace AliasAPI\Tests;

class CreatePairTest
{
    private $client;
    private $request;
    private $client_path;
    private $server_path;
    private $debug_path;
    private $new_request;
    private $say_path;

    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');

        require_once(dirname(__FILE__) . '/CreateClient.php');
    }

    public function testDeletePairFile()
    {
        // Delete the pair file because it is re-created in testCreatePairFile
        $request['actionS'] = 'delete pair file';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'Symbolic';

        $this->client = new CreateClient($request);

        $response = $this->client->sendRequest();

        says($this->client->tag, $this->client, $response);
    }

    public function testCreatePairFile()
    {
        $request['actionS'] = 'create pair file';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'Symbolic';

        $this->client = new CreateClient($request);

        $response = $this->client->sendRequest();

        $body = $response['body'];

        // Send the $body as a request so that the client creates the pair file
        $client = new CreateClient($body);
    }

    public function tearDown(): void
    {
        unset($this->client);
        // Delete previous result file.
        // Do not delete the files pair file here.
        // Most requests rely on the the pair files.
    }
}
