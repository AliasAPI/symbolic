<?php

namespace AliasAPI\Tests;

use AliasAPI\Client as Client;
use AliasAPI\CrudPair as CrudPair;
use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

/**
 * Class CreateClient
 * Autoloads files and configures a default request
 *
 * @package AliasAPI\Tests
 */
class CreateClient
{
    public $request = [];
    public $tag = '';

    /**
     * CreateClient autoloads files and sets additional default request parameters
     *
     * @param array $pod
     *
     * @return array
     */
    public function __construct($request)
    {
        require_once(dirname(__DIR__, 1) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'aliasapi' . DIRECTORY_SEPARATOR . 'frame' . DIRECTORY_SEPARATOR . 'client' . DIRECTORY_SEPARATOR . 'pipeline.php');

        $this->request = Client\pipeline($request);

        $this->setTag();
    }

    public function sendRequest()
    {
        $response = Messages\request($this->request);

        return $response;
    }

    public function getPair()
    {
        $pair = CrudPair\get_pair_globals();

        return $pair;
    }

    public function setTag()
    {
        $this->tag = Messages\get_tag_global();

        return $this->tag;
    }
}
