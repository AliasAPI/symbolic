<?php

declare(strict_types=1);

namespace AliasAPI\SymbolHistory;

use AliasAPI\CrudTable as CrudTable;

function download_symbol_lists($payload)
{
    if (! isset($payload['actionS'])
        || $payload['actionS'] !== 'create quotes') {
        return;
    }

    // SELECT MIN(updated) FROM symbol_list LIMIT 1

    // If it has been more than 12 hours
    // Download ftp.nasdaqtrader.com/SymbolDirectory/nasdaqlisted.txt to jsondata/
    // Download ftp.nasdaqtrader.com/SymbolDirectory/otherlisted.txt to jsondata/

    // To get the OTC symbols, visit https://otce.finra.org/otce/mp-list
    // Then click Download https://otce.finra.org/otce/directories
}
