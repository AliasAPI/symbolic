<?php

declare(strict_types=1);

namespace AliasAPI\Symbolic;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

/** 
 * Creates tables based on Y-m-d to store historical stock and ETF quotes
 *  
 * @param array $train 
 *
 * @return  array $train 
 */
function create_stock_quotes_table(array $train): array
{
    if (! isset($payload['action'])
        || $payload['action'] !== 'create stock quotes table') {
        return $train;
    }

    if (! \array_key_exists('table_name', $train) ) {
        Messages\respond(500, ["The table_name is not set for create_stock_quotes_table()"]);
    }

    // The pastquotes tables are YYYY-MM which helped speed
    $sql = "CREATE TABLE IF NOT EXISTS `" . $train['table_name'] . "` ( " .
           // The stock or ETF symbol
           "`symbol` varchar(11) NOT NULL, " .
           // The unadjusted open quote (or the quote at the datetime)
           "`open` decimal(8,5) NOT NULL DEFAULT '0.00', " .
           // The unadjusted low quote (or the quote at the datetime)
           "`low` decimal(8,5) NOT NULL DEFAULT '0.00', " .
           // The unadjusted high quote (or the quote at the datetime)
           "`high` decimal(8,5) NOT NULL DEFAULT '0.00', " .
           // The unadjusted close quote (or the quote at the datetime)
           "`close` decimal(8,5) NOT NULL DEFAULT '0.00', " .
           // The adjusted close quote (or the quote at the datetime)
           "`adj_close` decimal(8,5) NOT NULL DEFAULT '0.00', " .
           // The daily volume (or the volume at the datetime)
           "`volume` int(11) NOT NULL DEFAULT '0', " .
           // The Y-m-d h:i:s datetime
           "`datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00', " .
           // Use the key to help prevent duplicate data being stored
           "KEY `symbol` (`symbol`, `quote`, `datetime`), " .
           // Use the MyISAM table for read speed
           ") ENGINE=MyISAM ";

    CrudTable\query($sql);

    return $train;
}
