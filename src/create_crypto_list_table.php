<?php

declare(strict_types=1);

namespace AliasAPI\Symbolic;

use AliasAPI\CrudTable as CrudTable;

/**
 * Creates a table that stores a list of cryptocurrencies
 *
 * @param array $train
 *
 * @return  array $train
 */
function create_crypto_list_table(array $train): array
{
    if (! \array_key_exists('action', $train)
        || $train['action'] !== 'create crypto list') {
        return $train;
    }

    $sql = "CREATE TABLE IF NOT EXISTS `crypto_list` ( " .
            // The crypto symbol
            "`symbol` varchar(10) NOT NULL, " .
            // The full text name of the crypto
            "`name` varchar(100) NOT NULL, " .
            "`exchange` varchar(20) NOT NULL, " .
            // The source of the data (Coinbase, etc)
            "`source` varchar(20) NOT NULL, " .
            // A message if something is wrong with the symbol
            "`error` varchar(20) NOT NULL, " .
            "`updated` datetime DEFAULT CURRENT_TIMESTAMP, " .
            " PRIMARY KEY (`symbol`) " .
            // Use the MyISAM table for speed
            ") ENGINE=MyISAM ";

    CrudTable\query($sql);

    return $train;
}
