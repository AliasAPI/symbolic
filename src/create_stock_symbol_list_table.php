<?php

declare(strict_types=1);

namespace AliasAPI\Symbolic;

use AliasAPI\CrudTable as CrudTable;

function create_stock_symbol_list_table(array $train): array
{
    if (! isset($payload['action'])
        || $payload['action'] !== 'create stock symbol list table') {
        return $train;
    }

    $sql = "CREATE TABLE IF NOT EXISTS `symbol_list` ( " .
            // The stock or ETF symbol
            "`symbol` varchar(10) NOT NULL, " .
            // The full text name of the symbol
            "`name` varchar(100) NOT NULL, " .
            "`type` varchar(100) NOT NULL, " .
            // The amount of leverage. Stocks 1X and ETFs 1X, 1.25X, 2X, 3X, -2X, -3X
            "`leverage` varchar(10) NOT NULL, " .
            "`category` varchar(100) NOT NULL, " .
            "`industry` varchar(100) NOT NULL, " .
            "`class` varchar(100) NOT NULL, " .
            "`sector` varchar(100) NOT NULL, " .
            // OTC, NYSE,
            "`exchange` varchar(20) NOT NULL, " .
            // The source of the data (EODdata.com, Yahoo, etc)
            "`source` varchar(20) NOT NULL, " .
            // A message if something is wrong with the symbol
            "`error` varchar(20) NOT NULL, " .
            "`updated` datetime DEFAULT CURRENT_TIMESTAMP, " .
            " PRIMARY KEY (`symbol`) " .
            // Use the MyISAM table for speed
            ") ENGINE=MyISAM DEFAULT CHARSET=utf8mb4";

    CrudTable\query($sql);

    return $train;
}
