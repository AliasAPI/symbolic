<?php

declare(strict_types=1);

namespace AliasAPI\Symbolic;

use AliasAPI\Symbolic as Symbolic;
use AliasAPI\Messages as Messages;

function track(array $train): array
{
    # Make sure "@" is not in the symbol name

    $train = Symbolic\create_stock_symbol_list_table($train);
    
    $train = Symbolic\create_crypto_symbol_list_table($train);

    $train = Symbolic\download_symbol_lists($train);

    $train = Symbolic\import_symbol_lists($train);

    $train = Symbolic\select_symbols($train);

    $train = Symbolic\get_source1_quotes($train); // Download

    $train = Symbolic\format_source1_quotes($train);

    $train = Symbolic\create_stock_quotes_table($train);
    
    $train = Symbolic\create_crypto_quotes_table($train);

    $train = Symbolic\reduce_rows($train);

    Messages\respond(201, $train);
}
