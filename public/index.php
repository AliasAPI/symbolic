<?php

namespace AliasAPI\Items;

use AliasAPI\Api as API;
use AliasAPI\Items as Items;

require_once('../vendor/aliasapi/frame/api/bootstrap.php');

$settings = API\bootstrap();

$pod = API\pipeline($settings);
// 2DO+++ remove business logic from items?
Items\pipeline($pod);
