


When loading a table from a text file, use LOAD DATA. This is usually 20 times faster than using INSERT statements. See Section 13.2.6, “LOAD DATA Statement”.
https://dev.mysql.com/doc/refman/5.7/en/insert-optimization.html

LOAD DATA INFILE
To my surprise, LOAD DATA INFILE proves faster than a table copy:
LOAD DATA INFILE: 377,000 inserts / second
LOAD DATA LOCAL INFILE over the network: 322,000 inserts / second
https://medium.com/@benmorel/high-speed-inserts-with-mysql-9d3dcd76f723

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Symbolic creates a list of stocks and ETFs and downloads and validates (adjusted) quotes daily.

### How do I get set up? ###

* To set up Symbolic:
* git clone https://AliasAPI@bitbucket.org/AliasAPI/symbolic.git
* run composer.phar test command in the symbolic directory
* Edit .alias.json file in the symbolic/config directory

### Who do I talk to? ###

* Contact Drew Brown drew.brownjr@gmail.com with any questions.
